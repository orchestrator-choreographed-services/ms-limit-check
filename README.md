# Limit check

Checks the payment against pre-defined amount limits.
The following is list of limits per currency, default limit for not supported currencies is 100
 
| Currency | Limit | 
| :-------------: | :-------------: |
| AUD      | 2000 |
| USD     | 1000      |
| EGP | 20000     |

## Prerequisite

Before running this service please make sure you have the following

* Start all required services by following the steps in [Infrastructure Project](https://gitlab.com/orchestrator-choreographed-services/infrastructure)
* Make sure the required topics are pre-created in the Kafka cluster you are using

## Start the service

To start the service, you could use `gradle bootrun`.

## Publish payments

To publish PaymentCreated event, please use the scripts in [Infrastructure Project](https://gitlab.com/orchestrator-choreographed-services/infrastructure/blob/master/publish_payments.sh)