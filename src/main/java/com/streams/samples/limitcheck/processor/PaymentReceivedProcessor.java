package com.streams.samples.limitcheck.processor;

import com.streams.samples.limitcheck.core.PaymentAmountLimitChecker;
import com.streams.samples.limitcheck.model.LimitCheckCompleted;
import com.streams.samples.limitcheck.model.LimitCommandSignal;
import com.streams.samples.limitcheck.model.PaymentReceived;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.messaging.handler.annotation.SendTo;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Slf4j
@EnableBinding(PaymentReceivedProcessor.StreamProcessor.class)
public class PaymentReceivedProcessor {
    @Autowired
    private PaymentAmountLimitChecker paymentAmountLimitChecker;

    @StreamListener
    @SendTo("limitCheckCompleted")
    public KStream<String, LimitCheckCompleted> process(@Input("paymentReceived") KStream<String, PaymentReceived> paymentReceived,
                                                        @Input("limitCommandSignal") KStream<String, LimitCommandSignal> commandSignal) {
        paymentReceived.peek((k, v) -> {
            log.info("Received a payment received event with payment ID {}", k);
        });
        commandSignal.peek((k, v) -> {
            log.info("Received a command signal with payment ID {}", k);
        });

        JoinWindows joinWindow = JoinWindows.of(Duration.of(1, ChronoUnit.HOURS).toMillis());
        return paymentReceived.join(commandSignal, (v1, v2) -> v1, joinWindow, joined())
                .mapValues((k, v) -> paymentAmountLimitChecker.checkPayment(k, v));
    }

    private Joined<String, PaymentReceived, LimitCommandSignal> joined() {
        return Joined.with(new Serdes.StringSerde(), new JsonSerde<>(PaymentReceived.class), new JsonSerde<>(LimitCommandSignal.class));
    }

    public interface StreamProcessor {
        @Input("paymentReceived")
        KStream<?, ?> paymentReceived();

        @Input("limitCommandSignal")
        KStream<?, ?> limitCommandSignal();

        @Output("limitCheckCompleted")
        KStream<?, ?> limitCheckCompleted();
    }
}
