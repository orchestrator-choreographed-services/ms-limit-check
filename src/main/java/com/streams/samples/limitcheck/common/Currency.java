package com.streams.samples.limitcheck.common;

public enum Currency {
    AUD,
    USD,
    EGP
}
