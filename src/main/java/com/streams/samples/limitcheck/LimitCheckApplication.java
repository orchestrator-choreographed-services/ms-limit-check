package com.streams.samples.limitcheck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;

@SpringBootApplication
@EnableSchemaRegistryClient
public class LimitCheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(LimitCheckApplication.class, args);
	}

}
