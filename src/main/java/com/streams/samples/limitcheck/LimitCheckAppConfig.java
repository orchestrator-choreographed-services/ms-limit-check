package com.streams.samples.limitcheck;

import com.streams.samples.limitcheck.common.Money;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.schema.client.ConfluentSchemaRegistryClient;
import org.springframework.cloud.stream.schema.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LimitCheckAppConfig {
    @Value("${spring.cloud.stream.kafka.streams.binder.configuration.schema.registry.url}")
    private String endPoint;

    @Bean
    public SchemaRegistryClient schemaRegistryClient() {
        ConfluentSchemaRegistryClient client = new ConfluentSchemaRegistryClient();
        client.setEndpoint(endPoint);
        return client;
    }

    @Bean("currencyLimits")
    public Map<String, Money> currencyLimits() {
        return HashMap.of("AUD", Money.valueOf("2000"))
                .put("USD", Money.valueOf("1000"))
                .put("EGP", Money.valueOf("20000"));
    }
}
