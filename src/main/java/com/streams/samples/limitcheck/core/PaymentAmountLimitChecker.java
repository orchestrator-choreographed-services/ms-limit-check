package com.streams.samples.limitcheck.core;

import com.streams.samples.limitcheck.common.Money;
import com.streams.samples.limitcheck.model.LimitCheckCompleted;
import com.streams.samples.limitcheck.model.LimitCheckResultStatus;
import com.streams.samples.limitcheck.model.PaymentReceived;
import io.vavr.collection.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class PaymentAmountLimitChecker {
    @Autowired
    private Map<String, Money> currencyLimits;

    public LimitCheckCompleted checkPayment(String paymentId, PaymentReceived paymentReceived) {
        var checkResultStatus = LimitCheckResultStatus.NotExceed;
        var paymentAmount = Money.valueOf(paymentReceived.getAmount());
        var currencyLimitAmount = currencyLimitFor(paymentReceived.getCurrency().name());

        if (paymentAmount.compareTo(currencyLimitAmount) > 0) {
            checkResultStatus = LimitCheckResultStatus.Exceed;
        }

        return new LimitCheckCompleted(paymentId, checkResultStatus);
    }

    private Money currencyLimitFor(String currency) {
        return currencyLimits.getOrElse(currency, Money.valueOf("100"));
    }
}
