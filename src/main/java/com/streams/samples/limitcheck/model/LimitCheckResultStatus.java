package com.streams.samples.limitcheck.model;

public enum LimitCheckResultStatus {
    NotExceed,
    Exceed
}
