package com.streams.samples.limitcheck.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LimitCommandSignal {
    private String commandPayload;

    public LimitCommandSignal() {
    }

    public LimitCommandSignal(String commandPayload) {
        this.commandPayload = commandPayload;
    }

    public String getCommandPayload() {
        return commandPayload;
    }

    public void setCommandPayload(String commandPayload) {
        this.commandPayload = commandPayload;
    }
}
