package com.streams.samples.limitcheck.model;

import lombok.NonNull;
import lombok.Value;

@Value
public class LimitCheckCompleted {
    //This field is added only to support passing constant value to Zeebe. Zeebe connector supports extracting values from
    //fields, it does not support passing constant value
    @NonNull
    private final String paymentId;
    @NonNull
    private final LimitCheckResultStatus resultStatus;
    //Same as paymentId. This field is added only to support Zeebe connector
    @NonNull
    private final String eventType = "LimitCheckCompleted";
}
